#pragma once
#include <array>
#include <SFML\Graphics.hpp>
#include <Board\CheckersBoard.h>
#include <Renderers\SpriteRenderer\Piece.h>
#include <Interfaces\SpriteInterface.h>
#include <AI\MCTS.h>

class Game
{
private:
	sf::RenderWindow* m_pWindow;		// Window used for rendering, and input checks

	std::array<Piece, 24> m_pieces;		// Used for rendering the board's pieces

	CheckersBoard m_checkersBoard;		// Interal represenation of the board

	SpriteInterface m_spriteInterface;	// User interface to interal board

	MCTS m_mcts;						// Monte Carlo Tree Search AI opponent

	sf::Texture* m_pBoardTex;			// Texture which represents the checkers board
	sf::Texture* m_pPiecesTex;			// Texture which represents the checkers pieces

	sf::Sprite* m_pBoardSprite;			// Sprite which represents the checkers board

	bool m_hasFocus;					// Defines whether the window is in focus

	// Loads all resources required by the game
	void LoadResources();

	// Returns the pixel position of the mouse
	sf::Vector2f GetMousePosition();

	void Initialise();
	void Update();
	void Draw();

public:
	Game();
	~Game();

	void Run();
};
