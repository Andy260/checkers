#pragma once

struct Move;
class CheckersBoard;

class Player
{
private:
	CheckersBoard* m_pBoard;

	bool m_isPlayerOne;

public:
	Player(bool a_isPlayerOne);
	~Player();

	bool IsCurrentTurn();

	virtual void MakeMove(const Move& a_move);
};
