#include "SpriteRenderer.h"
#include <vector>

#include <Board\CheckersBoard.h>
#include <Renderers\SpriteRenderer\Piece.h>


SpriteRenderer::SpriteRenderer(CheckersBoard* a_pBoard)
	: m_pBoard(a_pBoard)
{
	m_pieces.resize(m_pBoard->GetSize() * m_pBoard->GetSize());
}

SpriteRenderer::~SpriteRenderer()
{
}

void SpriteRenderer::UpdatePieces()
{
	// Create moves vector, and allocate space for it
	std::vector<BoardMove> m_moves;
	m_moves.reserve(m_pBoard->GetSize() * m_pBoard->GetSize());

	m_pBoard->GetValidActions(m_moves);

	// Reset pieces
	for (unsigned int i = 0; i < m_pieces.size(); ++i)
	{
		Piece* pPiece = &m_pieces[i];
		pPiece->SetBoardPosition(PieceType::LIGHT_MEN, -1, -1);
	}

	unsigned int piecesItr = 0u;

	for (unsigned int x = 0; x < m_pBoard->GetSize(); ++x)
	{
		for (unsigned int y = 0; y < m_pBoard->GetSize(); ++y)
		{
			CellState cell = m_pBoard->GetCellState(x, y);

			if (cell == CellState::INVALID ||
				cell == CellState::UNOCCUPIED)
			{
				continue;
			}

			// Create piece for this cell
			Piece* pPiece = &m_pieces[piecesItr];
			switch (cell)
			{
			case CellState::LIGHT_MEN:
				pPiece->Initialise(PieceType::LIGHT_MEN, x, y);
				break;

			case CellState::LIGHT_KING:
				pPiece->Initialise(PieceType::LIGHT_KING, x, y);
				break;

			case CellState::DARK_MEN:
				pPiece->Initialise(PieceType::DARK_MEN, x, y);
				break;

			case CellState::DARK_KING:
				pPiece->Initialise(PieceType::DARK_KING, x, y);
				break;
			}

			pPiece->ResetMoves();

			// Add valid moves to this piece
			for (auto &move : m_moves)
			{
				// Not a move for this piece
				if (move.startCell.x != x ||
					move.startCell.y != y)
					continue;

				pPiece->AddMove(move);
			}

			piecesItr++;
		}
	}
}

void SpriteRenderer::Draw(float a_deltaTime)
{
	// TODO: Implement Draw() function
}
