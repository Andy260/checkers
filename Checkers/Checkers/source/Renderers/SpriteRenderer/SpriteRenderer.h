#pragma once
#include <vector>

class CheckersBoard;
class Piece;

class SpriteRenderer
{
private:
	CheckersBoard* m_pBoard;		// Reference to the checkers board

	bool m_isAnimating;				// Flags when an animation is occuring

	std::vector<Piece> m_pieces;	// List of piece objects

	void OnAnimationBegin();

	void AnimatePiece();

	// Should be called when the board state has changed
	void UpdatePieces();

	// Linerarly interpolates a piece from one position to another
	// Percentage, is how far into the lerp to move the piece to
	void LerpPiece(const sf::Vector2f& a_start, 
				   const sf::Vector2f& a_end, float a_percent);

public:
	SpriteRenderer(CheckersBoard* a_pBoard);
	~SpriteRenderer();

	void Draw(float a_deltaTime);

	inline bool IsAnimating() const { return m_isAnimating; }
};
