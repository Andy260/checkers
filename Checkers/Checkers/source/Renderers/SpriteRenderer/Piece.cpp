#include "Piece.h"
#include <SFML\Graphics.hpp>


sf::RectangleShape*	Piece::m_pLightMen_sprite(nullptr);
sf::RectangleShape*	Piece::m_pLightKing_sprite(nullptr);
sf::RectangleShape*	Piece::m_pDarkMen_sprite(nullptr);
sf::RectangleShape*	Piece::m_pDarkKing_sprite(nullptr);

sf::RectangleShape* Piece::m_pMoveHighlight(nullptr);
sf::Vector2f		Piece::m_boardUnit;

Piece::Piece()
	: m_shouldHighlightMoves(false)
{
	m_avaliableMoves.reserve(4);
}

void Piece::Initialise(const PieceType& a_type, unsigned int a_boardX, 
					   unsigned int a_boardY)
{
	m_boardPos	= sf::Vector2i(a_boardX, a_boardY);

	SetBoardPosition(a_type, a_boardX, a_boardY);
	ResetPosition();
}

void Piece::LoadResources(sf::Texture* a_pPieceTex, 
						  const sf::Vector2f& a_boardUnit)
{
	sf::Vector2u texSize = a_pPieceTex->getSize();

	// Create Light Men Sprite
	m_pLightMen_sprite = new sf::RectangleShape(a_boardUnit);
	m_pLightMen_sprite->setTexture(a_pPieceTex);
	m_pLightMen_sprite->setTextureRect(sf::IntRect(
		(texSize.x / 4) * 3,
		0,
		texSize.x / 4,
		texSize.y));

	// Create Light King Sprite
	m_pLightKing_sprite = new sf::RectangleShape(a_boardUnit);
	m_pLightKing_sprite->setTexture(a_pPieceTex);
	m_pLightKing_sprite->setTextureRect(sf::IntRect(
		(texSize.x / 4) * 1,
		0,
		texSize.x / 4,
		texSize.y));

	// Create Dark Men Sprite
	m_pDarkMen_sprite = new sf::RectangleShape(a_boardUnit);
	m_pDarkMen_sprite->setTexture(a_pPieceTex);
	m_pDarkMen_sprite->setTextureRect(sf::IntRect(
		(texSize.x / 4) * 2,
		0,
		texSize.x / 4,
		texSize.y));

	// Create Dark King Sprite
	m_pDarkKing_sprite = new sf::RectangleShape(a_boardUnit);
	m_pDarkKing_sprite->setTexture(a_pPieceTex);
	m_pDarkKing_sprite->setTextureRect(sf::IntRect(
		(texSize.x / 4) * 0,
		0,
		texSize.x / 4,
		texSize.y));

	// Create move highlighter shape
	m_boardUnit			= a_boardUnit;
	m_pMoveHighlight	= new sf::RectangleShape(m_boardUnit);
	m_pMoveHighlight->setFillColor(sf::Color(255, 0, 0, 255 / 2));
}

void Piece::UnloadResources()
{
	delete m_pLightMen_sprite;
	delete m_pLightKing_sprite;
	delete m_pDarkMen_sprite;
	delete m_pDarkKing_sprite;
}

sf::Vector2f Piece::GetBoardScreenPos(unsigned int a_x, unsigned int a_y)
{
	return sf::Vector2f(m_boardUnit.x * a_x, m_boardUnit.y * a_y);
}

void Piece::SetBoardPosition(const PieceType& a_type, int a_x, int a_y)
{
	m_type			= a_type;
	m_boardPos		= sf::Vector2i(a_x, a_y);
	m_normalPos		= sf::Vector2f(m_boardUnit.x * a_x, m_boardUnit.y * a_y);
}

sf::RectangleShape* Piece::GetSprite()
{
	sf::RectangleShape* pSprite = nullptr;

	// Get appropriate sprite for this pieces's type
	switch (m_type)
	{
	case PieceType::LIGHT_MEN:
		pSprite = m_pLightMen_sprite;
		break;

	case PieceType::LIGHT_KING:
		pSprite = m_pLightKing_sprite;
		break;

	case PieceType::DARK_MEN:
		pSprite = m_pDarkMen_sprite;
		break;

	case PieceType::DARK_KING:
		pSprite = m_pDarkKing_sprite;
		break;
	}

	return pSprite;
}

sf::FloatRect Piece::GetRectangle()
{
	return sf::FloatRect(m_currentPos.x, m_currentPos.y, m_boardUnit.x,
		m_boardUnit.y);
}

void Piece::Draw(sf::RenderWindow& a_window)
{
	// Don't draw this piece, because 
	// it doesn't represent a piece in play
	if (m_boardPos == sf::Vector2i(-1, -1))
	{
		return;
	}

	sf::RectangleShape* pSprite = GetSprite();

	// Draw move highlights
	if (m_shouldHighlightMoves)
	{
		for (auto &i : m_avaliableMoves)
		{
			m_pMoveHighlight->setPosition(i.rect.left, i.rect.top);
			a_window.draw(*m_pMoveHighlight);
		}
	}

	// Draw piece sprite
	if (m_currentPos != m_normalPos)
	{
		pSprite->setOrigin(m_boardUnit.x / 2.0f, m_boardUnit.y / 2.0f);
	}
	else
	{
		pSprite->setOrigin(0.0f, 0.0f);
	}

	pSprite->setPosition(m_currentPos);
	a_window.draw(*pSprite);
}

void Piece::AddMove(const BoardMove& a_move)
{
	// Prepare move variables
	sf::Vector2f moveBoardPos = GetBoardScreenPos(a_move.endCell.x, 
												  a_move.endCell.y);
	sf::FloatRect rect(moveBoardPos, m_boardUnit);

	// Create move
	PieceMove move
	{
		rect, a_move
	};

	m_avaliableMoves.push_back(move);
}

void Piece::ResetMoves()
{
	m_avaliableMoves.clear();
}
