#pragma once
#include <vector>
#include <SFML\Graphics\Rect.hpp>
#include <SFML\System\Vector2.hpp>
#include <Board\CheckersBoard.h>

namespace sf { class Sprite; class Texture; class RenderWindow;  class RectangleShape; }

enum class PieceType
{
	LIGHT_MEN,
	LIGHT_KING,
	DARK_MEN,
	DARK_KING
};

struct PieceMove
{
	sf::FloatRect rect;	// Used for mouse position calculations
	BoardMove move;		// Associated internal board move
};

class Piece
{
private:
	static sf::RectangleShape* m_pLightMen_sprite;	// Light Men Sprite
	static sf::RectangleShape* m_pLightKing_sprite;	// Light King Sprite
	static sf::RectangleShape* m_pDarkMen_sprite;	// Dark Men Sprite
	static sf::RectangleShape* m_pDarkKing_sprite;	// Dark King Sprite

	static sf::RectangleShape* m_pMoveHighlight;	// Highlights all possible moves of this piece

	static sf::Vector2f m_boardUnit;				// Used for spacing sprites

	PieceType m_type;								// Represents this pieces type on the board

	sf::Vector2f m_normalPos;						// Normal window position for dragging pieces
	sf::Vector2f m_currentPos;

	sf::Vector2i m_boardPos;						// Internal position within the board (board cell)

	std::vector<PieceMove> m_avaliableMoves;		// List of all moves this piece is allowed to make

	bool m_shouldHighlightMoves;					// Will highlight moves with rectangle shape

	// Returns the releveant sprite, dependant on the type of piece
	sf::RectangleShape* GetSprite();

	// Returns the position of this board cell, in screen space
	sf::Vector2f GetBoardScreenPos(unsigned int a_x, unsigned int a_y);

public:
	Piece();

	void Initialise(const PieceType& a_type, unsigned int a_boardX, 
					unsigned int a_boardY);

	// Loads all resources required for this class
	static void LoadResources(sf::Texture* a_pPieceTex, 
							  const sf::Vector2f& a_boardUnit);

	// Unloads all resources used by this class
	static void UnloadResources();

	// Sets the position of this piece within the board
	void SetBoardPosition(const PieceType& a_type, int a_x, int a_y);

	// Reste the position of this piece in screen space, 
	// back to screen space's board position
	inline void ResetPosition() { m_currentPos = m_normalPos; }

	// Sets the position of this piece in screen space
	inline void SetPosition(const sf::Vector2f& a_pos) { m_currentPos = a_pos; }

	// Used for calculating if mouse is over this piece
	sf::FloatRect GetRectangle();

	// Adds a move for GUI System's comparisons
	void AddMove(const BoardMove& a_move);

	// Removes all moves given to this piece
	void ResetMoves();

	void Draw(sf::RenderWindow& a_window);

	inline sf::Vector2i GetBoardPosition() const { return m_boardPos; }

	// Returns all the given moves for this piece
	inline const std::vector<PieceMove>& GetMoves() const { return m_avaliableMoves; }

	inline void ShouldHighlightMoves(bool a_value) { m_shouldHighlightMoves = a_value; }
};
