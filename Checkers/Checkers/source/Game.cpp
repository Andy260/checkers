#include "Game.h"
#include <iostream>


Game::Game()
	: m_spriteInterface(&m_checkersBoard, &m_pieces, State::PLAYER_ONE),
	m_mcts(1000, State::PLAYER_TWO)
{
	
}

Game::~Game()
{
	delete m_pBoardTex;
	delete m_pBoardSprite;
}

void Game::Initialise()
{
	LoadResources();

	// Calculate board unit for correct rendering of pieces
	sf::Vector2u boardTex_size		= m_pBoardTex->getSize();
	sf::Vector2f boardSprite_scale	= m_pBoardSprite->getScale();
	sf::Vector2f boardUnit(
		((float)boardTex_size.x / 2) * boardSprite_scale.x,
		((float)boardTex_size.y / 2) * boardSprite_scale.y);

	Piece::LoadResources(m_pPiecesTex, boardUnit);

	sf::Vector2i windowSize((int)boardUnit.x * (int)m_checkersBoard.GetSize(), 
							(int)boardUnit.y * (int)m_checkersBoard.GetSize());
	m_pWindow = new sf::RenderWindow(sf::VideoMode(windowSize.x, windowSize.y), 
									 "Checkers", sf::Style::Close);

	m_spriteInterface.Initialise();
}

void Game::Update()
{
#if !DEBUG
	if (!m_hasFocus)
	{
		return;
	}
#endif

	sf::Vector2f mousePos = GetMousePosition();

	m_mcts.Update(m_checkersBoard);
	m_spriteInterface.Update(mousePos);
}

void Game::Draw()
{
	m_pWindow->clear(sf::Color::Black);

	// Draw board
	m_pWindow->draw(*m_pBoardSprite);

	// Draw pieces
	for (auto &i : m_pieces)
	{
		i.Draw(*m_pWindow);
	}
}

sf::Vector2f Game::GetMousePosition()
{
	sf::Vector2i pixelPos = sf::Mouse::getPosition(*m_pWindow);
	sf::Vector2f coordPos = m_pWindow->mapPixelToCoords(pixelPos);

	return sf::Vector2f(coordPos.x, coordPos.y);
}

void Game::LoadResources()
{
	m_pBoardTex		= new sf::Texture();
	m_pPiecesTex	= new sf::Texture();

	bool successful = true;

	if (!m_pBoardTex->loadFromFile("./textures/boardTile.png"))
	{
		std::cout << "Unable to load board texture..." << std::endl;
		successful = false;
	}

	if (!m_pPiecesTex->loadFromFile("./textures/pieces.png"))
	{
		std::cout << "Unable to load pieces texture..." << std::endl;
		successful = false;
	}

	if (!successful)
		exit(EXIT_FAILURE);

	// Setup textures
	m_pBoardTex->setSmooth(true);
	m_pBoardTex->setRepeated(true);
	m_pPiecesTex->setSmooth(true);

	sf::Vector2u boardTex_size = m_pBoardTex->getSize();

	// Create board Sprite
	m_pBoardSprite = new sf::Sprite(*m_pBoardTex, sf::IntRect(
		0,
		0,
		boardTex_size.x * 4,
		boardTex_size.y * 4));
}

void Game::Run()
{
	Initialise();

	while (m_pWindow->isOpen())
	{
		sf::Event event;
		while (m_pWindow->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				m_pWindow->close();

			if (event.type == sf::Event::GainedFocus)
				m_hasFocus = true;

			if (event.type == sf::Event::LostFocus)
				m_hasFocus = false;
		}

		Update();
		Draw();
		m_pWindow->display();
	}
}
