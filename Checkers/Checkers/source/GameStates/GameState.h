#pragma once

namespace sf { class RenderWindow; }

class Game;

class GameState
{
protected:
	Game* m_pGame;

public:
	GameState(Game* a_pGame) 
		: m_pGame(a_pGame) {}
	virtual ~GameState() {}

	virtual void Initialise()							= 0;
	virtual void Update()								= 0;
	virtual void Draw(const sf::RenderWindow& a_window) = 0;
};