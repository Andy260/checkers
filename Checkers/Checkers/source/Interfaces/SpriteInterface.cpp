#include "SpriteInterface.h"

// STL includes
#include <assert.h>
#include <iostream>

// Project includes
#include <Players\Player.h>
#include <Board\CheckersBoard.h>
#include <Renderers\SpriteRenderer\Piece.h>
#include <SFML\Window\Mouse.hpp>
#include <SFML\Graphics\Rect.hpp>


SpriteInterface::SpriteInterface(CheckersBoard* a_pBoard, std::array<Piece, 24>* a_pPieces,
					 const State& a_player)
	: m_pPieces(a_pPieces),
	  m_pBoard(a_pBoard),
	  m_lastMousePress(false),
	  m_playerState(a_player)
{
	assert(a_player != State::DRAW);
}

SpriteInterface::~SpriteInterface()
{
}

void SpriteInterface::Initialise()
{
	UpdatePieces();

	m_lastPlayerTurn = m_pBoard->GetCurrentTurn();
}

void SpriteInterface::Update(const sf::Vector2f& a_mousePos)
{
	// Should only call this if board has changed...
	UpdatePieces();

	if (m_pBoard->GetCurrentTurn() != m_playerState)
	{
		// Not this player's turn
		return;
	}

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		if (!m_lastMousePress)
		{
			OnMousePress(a_mousePos);
		}
		else
		{
			OnMouseHeld(a_mousePos);
		}

		m_lastMousePress = true;
	}
	else if (!sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_lastMousePress)
	{
		OnMouseRelease(a_mousePos);
		m_lastMousePress = false;
	}
}

void SpriteInterface::OnMousePress(const sf::Vector2f& a_mousePos)
{
	m_pHoldingPiece = nullptr;

	// Check if the mouse is over a piece, and pick it up
	for (auto i = m_pPieces->begin(); i < m_pPieces->end(); ++i)
	{
		unsigned int boardX = i->GetBoardPosition().x;
		unsigned int boardY = i->GetBoardPosition().y;

		sf::FloatRect pieceRect = (*i).GetRectangle();

		if (pieceRect.contains(a_mousePos))
		{
			// Assign this piece to hold, and show valid moves
			m_pHoldingPiece = &(*i);
			m_pHoldingPiece->ShouldHighlightMoves(true);

			break;
		}
	}
}

void SpriteInterface::OnMouseHeld(const sf::Vector2f& a_mousePos)
{
	if (m_pHoldingPiece == nullptr)
		return;

	// Move piece to mouse position
	m_pHoldingPiece->SetPosition(a_mousePos);
}

void SpriteInterface::OnMouseRelease(const sf::Vector2f& a_mousePos)
{
	// No piece is being held by the player
	if (m_pHoldingPiece == nullptr)
	{
		return;
	}

	ApplyMove(a_mousePos);

	m_pHoldingPiece->ResetPosition();
	m_pHoldingPiece->ShouldHighlightMoves(false);

	m_pHoldingPiece = nullptr;
}

void SpriteInterface::ApplyMove(const sf::Vector2f& a_mousePos)
{
	const std::vector<PieceMove> moves = m_pHoldingPiece->GetMoves();

	for (auto &i : moves)
	{
		// Check if mouse is within a valid move rectangle
		if (i.rect.contains(a_mousePos))
		{
			// Apply action onto internal board and update rendering
			m_pBoard->PerformAction(i.move);
			UpdatePieces();
			break;
		}
	}
}

void SpriteInterface::UpdatePieces()
{
	// Create moves vector, and allocate space for it
	std::vector<BoardMove> m_moves;
	m_moves.reserve(m_pBoard->GetSize() * m_pBoard->GetSize());

	m_pBoard->GetValidActions(m_moves);

	// Reset pieces
	for (unsigned int i = 0; i < m_pPieces->size(); ++i)
	{
		Piece* pPiece = &m_pPieces->at(i);
		pPiece->SetBoardPosition(PieceType::LIGHT_MEN, -1, -1);
	}

	unsigned int piecesItr = 0u;

	for (unsigned int x = 0; x < m_pBoard->GetSize(); ++x)
	{
		for (unsigned int y = 0; y < m_pBoard->GetSize(); ++y)
		{
			CellState cell = m_pBoard->GetCellState(x, y);

			if (cell == CellState::INVALID ||
				cell == CellState::UNOCCUPIED)
			{
				continue;
			}

			// Create piece for this cell
			Piece* pPiece = &m_pPieces->at(piecesItr);
			switch (cell)
			{
			case CellState::LIGHT_MEN:
				pPiece->Initialise(PieceType::LIGHT_MEN, x, y);
				break;

			case CellState::LIGHT_KING:
				pPiece->Initialise(PieceType::LIGHT_KING, x, y);
				break;

			case CellState::DARK_MEN:
				pPiece->Initialise(PieceType::DARK_MEN, x, y);
				break;

			case CellState::DARK_KING:
				pPiece->Initialise(PieceType::DARK_KING, x, y);
				break;
			}

			pPiece->ResetMoves();

			// Add valid moves to this piece
			for (auto &move : m_moves)
			{
				// Not a move for this piece
				if (move.startCell.x != x ||
					move.startCell.y != y)
					continue;

				pPiece->AddMove(move);
			}

			piecesItr++;
		}
	}
}
