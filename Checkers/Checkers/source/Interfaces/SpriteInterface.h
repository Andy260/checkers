#pragma once
#include <array>
#include <SFML\System\Vector2.hpp>

class Player;
class CheckersBoard;
class Piece;
enum class State;

class SpriteInterface
{
private:
	State m_playerState;				// Which player on the board, this system controls

	State m_lastPlayerTurn;				// For checking when the interface should update after a board change

	CheckersBoard* m_pBoard;			// Board representation
	std::array<Piece, 24>* m_pPieces;	// List of all pieces

	Piece* m_pHoldingPiece;				// The piece which is currently in user's hand

	bool m_lastMousePress;				// State of left mouse press, of the previous frame

	// Sends the desired move to the board state
	void ApplyMove(const sf::Vector2f& a_mousePos);

	// Event that should be raised on the first frame the
	// left mouse button is pressed
	void OnMousePress(const sf::Vector2f& a_mousePos);

	// Event that should be raised every from onwards
	// of the first frame the left mouse button is pressed
	void OnMouseHeld(const sf::Vector2f& a_mousePos);

	// Event that should be raised on the frame which
	// the left mouse button is released
	void OnMouseRelease(const sf::Vector2f& a_mousePos);

	// Updates board rendering (should be moved into relevant class)
	void UpdatePieces();

public:
	SpriteInterface(CheckersBoard* a_pBoard, std::array<Piece, 24>* a_pPieces,
			  const State& a_player);
	~SpriteInterface();

	void Initialise();

	void Update(const sf::Vector2f& a_mousePos);
};
