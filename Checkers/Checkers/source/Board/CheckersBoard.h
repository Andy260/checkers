#pragma once
#include <array>
#include <vector>
#include <SFML\System\Vector2.hpp>

// Defines which player is currently winning in this board state
// of if a winning has been found. Is also used to define which
// player's turn it currently is
enum class State
{
	PLAYER_ONE,
	PLAYER_TWO,
	DRAW,
	UNKNOWN,
};

// Represents a move/action to make on a checkers board
struct BoardMove
{
	sf::Vector2i startCell;
	sf::Vector2i endCell;
	sf::Vector2i jumpCell;

	BoardMove() : jumpCell(-1, -1) {}
};

// Represents the state of a position within the board
enum class CellState : char
{
	INVALID,
	UNOCCUPIED,
	LIGHT_MEN,
	LIGHT_KING,
	DARK_MEN,
	DARK_KING
};

struct PieceCount
{
	unsigned int lightMen;
	unsigned int lightKing;
	unsigned int darkMen;
	unsigned int darkKing;
};

class CheckersBoard
{
private:
	std::array<CellState, 64> m_boardState;		// Representation of the board

	State m_currentTurn;						// Defines which player's turn it is

	sf::Vector2i m_lastJumpPosition;			// Used for checking for multiple jump moves

	BoardMove m_lastMove;						// Defines what the last move was

	// Returns an iterator to the requested cell (doesn't account for out of bounds)
	unsigned int GetCellItr(const sf::Vector2i& a_cell) const;

	// Sets the cell at the requested position
	void SetBoardPositionState(int a_x, int a_y,
							   const CellState& a_state);

	// Generates all valid normal moves for GetValidActions() move checks
	void CreateNormalMoves(int a_x, int a_y, bool a_isPlayerOne, BoardMove& a_leftUp,
						   BoardMove& a_rightUp, BoardMove& a_leftDown,
						   BoardMove& a_rightDown) const;

	// Generates all valid Jump moves for GetValidActions() move checks
	void CreateJumpMoves(int a_x, int a_y, bool a_isPlayerOne, BoardMove& a_upLeft_jump, 
						 BoardMove& a_upRight_jump,  BoardMove& a_downLeft_jump, 
						 BoardMove& a_downRight_jump) const;

	// Returns whether or not a valid 
	bool IsValidMove(const BoardMove& a_move, const CellState& a_menState,
					 const CellState& a_kingState) const;

public:
	CheckersBoard();

	// Returns all valid moves for the current player, in this state
	// (also updates the turn state)
	void GetValidActions(std::vector<BoardMove>& a_actions);

	// Performs an action on this board state
	void PerformAction(const BoardMove& a_move);

	// Returns the current state of a cell
	CellState GetCellState(const sf::Vector2i& a_cell) const;
	CellState GetCellState(int a_x, int a_y) const;

	// Returns which player is currently winning in this state
	State GetState();

	// Returns current piece count
	PieceCount GetPieceCount() const;

	// Clones this board state (used within MCTS AIs)
	CheckersBoard* Clone() const;

	// Returns the total number of collums/rows
	inline unsigned int GetSize() const { return 8u; }

	// Returns which player's turn it currently is its current state
	inline State GetCurrentTurn() const { return m_currentTurn; }

	inline BoardMove GetLastMove() const { return m_lastMove; }
};
