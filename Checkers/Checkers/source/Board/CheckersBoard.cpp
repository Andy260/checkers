#include "CheckersBoard.h"
#include <iostream>
#include <assert.h>
#include <SFML\Graphics.hpp>


CheckersBoard::CheckersBoard() : 
	m_currentTurn(State::PLAYER_ONE),
	m_lastJumpPosition(-1, -1)
{
	// Create initial board layout
	m_boardState[0]		= CellState::INVALID;
	m_boardState[1]		= CellState::LIGHT_MEN;
	m_boardState[2]		= CellState::INVALID;
	m_boardState[3]		= CellState::LIGHT_MEN;
	m_boardState[4]		= CellState::INVALID;
	m_boardState[5]		= CellState::LIGHT_MEN;
	m_boardState[6]		= CellState::INVALID;
	m_boardState[7]		= CellState::LIGHT_MEN;

	m_boardState[8]		= CellState::LIGHT_MEN;
	m_boardState[9]		= CellState::INVALID;
	m_boardState[10]	= CellState::LIGHT_MEN;
	m_boardState[11]	= CellState::INVALID;
	m_boardState[12]	= CellState::LIGHT_MEN;
	m_boardState[13]	= CellState::INVALID;
	m_boardState[14]	= CellState::LIGHT_MEN;
	m_boardState[15]	= CellState::INVALID;

	m_boardState[16]	= CellState::INVALID;
	m_boardState[17]	= CellState::LIGHT_MEN;
	m_boardState[18]	= CellState::INVALID;
	m_boardState[19]	= CellState::LIGHT_MEN;
	m_boardState[20]	= CellState::INVALID;
	m_boardState[21]	= CellState::LIGHT_MEN;
	m_boardState[22]	= CellState::INVALID;
	m_boardState[23]	= CellState::LIGHT_MEN;

	m_boardState[24]	= CellState::UNOCCUPIED;
	m_boardState[25]	= CellState::INVALID;
	m_boardState[26]	= CellState::UNOCCUPIED;
	m_boardState[27]	= CellState::INVALID;
	m_boardState[28]	= CellState::UNOCCUPIED;
	m_boardState[29]	= CellState::INVALID;
	m_boardState[30]	= CellState::UNOCCUPIED;
	m_boardState[31]	= CellState::INVALID;

	m_boardState[32]	= CellState::INVALID;
	m_boardState[33]	= CellState::UNOCCUPIED;
	m_boardState[34]	= CellState::INVALID;
	m_boardState[35]	= CellState::UNOCCUPIED;
	m_boardState[36]	= CellState::INVALID;
	m_boardState[37]	= CellState::UNOCCUPIED;
	m_boardState[38]	= CellState::INVALID;
	m_boardState[39]	= CellState::UNOCCUPIED;

	m_boardState[40]	= CellState::DARK_MEN;
	m_boardState[41]	= CellState::INVALID;
	m_boardState[42]	= CellState::DARK_MEN;
	m_boardState[43]	= CellState::INVALID;
	m_boardState[44]	= CellState::DARK_MEN;
	m_boardState[45]	= CellState::INVALID;
	m_boardState[46]	= CellState::DARK_MEN;
	m_boardState[47]	= CellState::INVALID;

	m_boardState[48]	= CellState::INVALID;
	m_boardState[49]	= CellState::DARK_MEN;
	m_boardState[50]	= CellState::INVALID;
	m_boardState[51]	= CellState::DARK_MEN;
	m_boardState[52]	= CellState::INVALID;
	m_boardState[53]	= CellState::DARK_MEN;
	m_boardState[54]	= CellState::INVALID;
	m_boardState[55]	= CellState::DARK_MEN;

	m_boardState[56]	= CellState::DARK_MEN;
	m_boardState[57]	= CellState::INVALID;
	m_boardState[58]	= CellState::DARK_MEN;
	m_boardState[59]	= CellState::INVALID;
	m_boardState[60]	= CellState::DARK_MEN;
	m_boardState[61]	= CellState::INVALID;
	m_boardState[62]	= CellState::DARK_MEN;
	m_boardState[63]	= CellState::INVALID;
}

unsigned int CheckersBoard::GetCellItr(const sf::Vector2i& a_cell) const
{
	unsigned int size = GetSize();

	return a_cell.y * GetSize() + a_cell.x;
}

CellState CheckersBoard::GetCellState(const sf::Vector2i& a_cell) const
{
	return GetCellState(a_cell.x, a_cell.y);
}

CellState CheckersBoard::GetCellState(int a_x, int a_y) const
{
	if (a_x < 0 || a_y < 0 ||
		a_x > (int)GetSize() - 1 || a_y > (int)GetSize() - 1)
	{
		return CellState::INVALID;
	}

	return m_boardState[GetCellItr(sf::Vector2i(a_x, a_y))];
}

State CheckersBoard::GetState()
{
	// Calculate amount of valid moves
	std::vector<BoardMove> avaliableMoves;
	GetValidActions(avaliableMoves);

	// There are still valid moves
	if (avaliableMoves.size() != 0)
	{
		return State::UNKNOWN;
	}

	// No valid moves, choose a winner
	if (m_currentTurn == State::PLAYER_ONE)
	{
		return State::PLAYER_TWO;
	}
	else
	{
		return State::PLAYER_TWO;
	}
}

PieceCount CheckersBoard::GetPieceCount() const
{
	PieceCount count = { 0, 0, 0, 0 };

	for (auto &i : m_boardState)
	{
		if (i == CellState::INVALID ||
			i == CellState::UNOCCUPIED)
		{
			continue;
		}

		switch (i)
		{
		case CellState::LIGHT_MEN:
			count.lightMen++;
			break;

		case CellState::LIGHT_KING:
			count.lightKing++;
			break;

		case CellState::DARK_MEN:
			count.darkMen++;
			break;

		case CellState::DARK_KING:
			count.darkKing++;
			break;
		}
	}
	
	return count;
}

void CheckersBoard::CreateNormalMoves(int a_x, int a_y, bool a_isPlayerOne, BoardMove& a_leftUp,
	BoardMove& a_rightUp, BoardMove& a_leftDown,
	BoardMove& a_rightDown) const
{
	// Should work for both players
	sf::Vector2i moveDelta(1, 1);
	if (!a_isPlayerOne)
	{
		moveDelta.x = -1;
		moveDelta.y = -1;
	}

	// Create left up move
	a_leftUp.startCell		= sf::Vector2i(a_x, a_y);
	a_leftUp.endCell		= sf::Vector2i(a_x - (1 * moveDelta.x),
										   a_y - (1 * moveDelta.y));

	// Create right up move
	a_rightUp.startCell		= sf::Vector2i(a_x, a_y);
	a_rightUp.endCell		= sf::Vector2i(a_x + (1 * moveDelta.x),
										   a_y - (1 * moveDelta.y));

	// Create left-down move (king move)
	a_leftDown.startCell	= sf::Vector2i(a_x, a_y);
	a_leftDown.endCell		= sf::Vector2i(a_x - (1 * moveDelta.x),
										   a_y + (1 * moveDelta.y));

	// Create right-down move (king move)
	a_rightDown.startCell	= sf::Vector2i(a_x, a_y);
	a_rightDown.endCell		= sf::Vector2i(a_x + (1 * moveDelta.x),
										   a_y + (1 * moveDelta.y));
}

void CheckersBoard::CreateJumpMoves(int a_x, int a_y, bool a_isPlayerOne, BoardMove& a_upLeft_jump,
	BoardMove& a_upRight_jump, BoardMove& a_downLeft_jump,
	BoardMove& a_downRight_jump) const
{
	// Should work for both players
	sf::Vector2i moveDelta(1, 1);
	if (!a_isPlayerOne)
	{
		moveDelta.x = -1;
		moveDelta.y = -1;
	}

	// Create jump up-left move
	a_upLeft_jump.startCell		= sf::Vector2i(a_x, a_y);
	a_upLeft_jump.jumpCell		= sf::Vector2i(a_x - (1 * moveDelta.x),
											   a_y - (1 * moveDelta.y));
	a_upLeft_jump.endCell		= sf::Vector2i(a_x - (2 * moveDelta.x),
											   a_y - (2 * moveDelta.y));

	// Create jump up-right move
	a_upRight_jump.startCell	= sf::Vector2i(a_x, a_y);
	a_upRight_jump.jumpCell		= sf::Vector2i(a_x + (1 * moveDelta.x),
											   a_y - (1 * moveDelta.y));
	a_upRight_jump.endCell		= sf::Vector2i(a_x + (2 * moveDelta.x),
											   a_y - (2 * moveDelta.y));

	// Create jump left-down move (king move)
	a_downLeft_jump.startCell	= sf::Vector2i(a_x, a_y);
	a_downLeft_jump.jumpCell	= sf::Vector2i(a_x - (1 * moveDelta.y),
											   a_y + (1 * moveDelta.y));
	a_downLeft_jump.endCell		= sf::Vector2i(a_x - (2 * moveDelta.x),
											   a_y + (2 * moveDelta.y));

	// Create jump right-down move (king move)
	a_downRight_jump.startCell	= sf::Vector2i(a_x, a_y);
	a_downRight_jump.jumpCell	= sf::Vector2i(a_x + (1 * moveDelta.x),
											   a_y + (1 * moveDelta.y));
	a_downRight_jump.endCell	= sf::Vector2i(a_x + (2 * moveDelta.x),
											   a_y + (2 * moveDelta.y));
}

void CheckersBoard::GetValidActions(std::vector<BoardMove>& a_actions)
{
	State currentTurn = GetCurrentTurn();

	// No moves can be made if draw
	if (currentTurn == State::DRAW)
	{
		return;
	}

	// Create lists to contain valid moves
	std::vector<BoardMove> jumpMoves;
	std::vector<BoardMove> normalMoves;
	jumpMoves.reserve(GetSize() * GetSize());
	normalMoves.reserve(GetSize() * GetSize());

	// Check which player's turn it is
	bool isPlayerOne_turn = currentTurn == State::PLAYER_ONE ? true : false;

	// Create moves to check
	BoardMove moveLeftUp, moveRightUp, moveLeftDown, moveRightDown, 
			  moveLeftUp_jump,  moveRightUp_jump, moveLeftDown_jump, 
			  moveRightDown_jump;

	// Establish which player's pieces we're
	// concerned with
	CellState menState, kingState;
	if (isPlayerOne_turn)
	{
		menState	= CellState::DARK_MEN;
		kingState	= CellState::DARK_KING;
	}
	else
	{
		menState	= CellState::LIGHT_MEN;
		kingState	= CellState::LIGHT_KING;
	}

	// Check for any jump moves connected to last jump move
	if (m_lastJumpPosition != sf::Vector2i(-1, -1))
	{
		CellState cell = GetCellState(m_lastJumpPosition.x, m_lastJumpPosition.y);

		CreateJumpMoves(m_lastJumpPosition.x, m_lastJumpPosition.y, isPlayerOne_turn, 
			moveLeftUp_jump, moveRightUp_jump, moveLeftDown_jump, moveRightDown_jump);

		// Check for normal jump
		if (IsValidMove(moveLeftUp_jump, menState, kingState))
		{
			jumpMoves.push_back(moveLeftUp_jump);
		}

		if (IsValidMove(moveRightUp_jump, menState, kingState))
		{
			jumpMoves.push_back(moveRightUp_jump);
		}

		if (cell == kingState)
		{
			// Check for king jump
			if (IsValidMove(moveLeftDown_jump, menState, kingState))
			{
				jumpMoves.push_back(moveLeftDown_jump);
			}

			if (IsValidMove(moveRightDown_jump, menState, kingState))
			{
				jumpMoves.push_back(moveRightDown_jump);
			}
		}

		if (jumpMoves.size() > 0)
		{
			// Player must make next jump move(s)
			a_actions = jumpMoves;
			return;
		}

		// No jump moves found for last move
		// recheck for next player's move
		m_lastJumpPosition.x = -1;
		m_lastJumpPosition.y = -1;

		m_currentTurn = 
			m_currentTurn == State::PLAYER_ONE ? State::PLAYER_TWO : State::PLAYER_ONE;

		GetValidActions(a_actions);
		return;
	}

	for (unsigned int x = 0; x < GetSize(); ++x)
	{
		for (unsigned int y = 0; y < GetSize(); ++y)
		{
			CellState cell = GetCellState(x, y);

			// Ignore unoccupied cells, or cells taken
			// by pieces not owned by current player
			if (cell == CellState::UNOCCUPIED	||
				cell == CellState::INVALID		||
				(cell != menState && cell != kingState))
			{
				continue;
			}

			// Create moves for this cell
			CreateNormalMoves(x, y, isPlayerOne_turn, moveLeftUp, moveRightUp,
				moveLeftDown, moveRightDown);

			CreateJumpMoves(x, y, isPlayerOne_turn, moveLeftUp_jump,
				moveRightUp_jump, moveLeftDown_jump,
				moveRightDown_jump);

			// Check for normal movement
			if (IsValidMove(moveLeftUp, menState, kingState))
			{
				normalMoves.push_back(moveLeftUp);
			}

			if (IsValidMove(moveRightUp, menState, kingState))
			{
				normalMoves.push_back(moveRightUp);
			}

			// Check for normal jump
			if (IsValidMove(moveLeftUp_jump, menState, kingState))
			{
				jumpMoves.push_back(moveLeftUp_jump);
			}

			if (IsValidMove(moveRightUp_jump, menState, kingState))
			{
				jumpMoves.push_back(moveRightUp_jump);
			}

			// Check for backwards movement if king
			if (cell == kingState)
			{
				if (IsValidMove(moveLeftDown, menState, kingState))
				{
					normalMoves.push_back(moveLeftDown);
				}

				if (IsValidMove(moveRightDown, menState, kingState))
				{
					normalMoves.push_back(moveRightDown);
				}

				// Check for king jump
				if (IsValidMove(moveLeftDown_jump, menState, kingState))
				{
					jumpMoves.push_back(moveLeftDown_jump);
				}

				if (IsValidMove(moveRightDown_jump, menState, kingState))
				{
					jumpMoves.push_back(moveRightDown_jump);
				}
			}
		}
	}

	// Return jump moves only if any exist
	a_actions = jumpMoves.size() > 0 ? jumpMoves : normalMoves;

	// Declare draw if no valid moves can be made
	if (a_actions.size() == 0)
	{
		m_currentTurn = State::DRAW;
	}
}

bool CheckersBoard::IsValidMove(const BoardMove& a_move,
								const CellState& a_menState,
								const CellState& a_kingState) const
{
	CellState startCellState = GetCellState(a_move.startCell);
	CellState endCellState	 = GetCellState(a_move.endCell);

	assert(startCellState != CellState::INVALID &&
		   startCellState != CellState::UNOCCUPIED);

	// Check for valid jump move
	if (a_move.jumpCell != sf::Vector2i(-1, -1))
	{
		CellState jumpCellState = GetCellState(a_move.jumpCell.x, 
											   a_move.jumpCell.y);

		if (jumpCellState == CellState::INVALID		|| 
			jumpCellState == CellState::UNOCCUPIED	||
			jumpCellState == a_menState				||
			jumpCellState == a_kingState)
		{
			return false;
		}
	}

	return endCellState == CellState::UNOCCUPIED;
}

void CheckersBoard::PerformAction(const BoardMove& a_move)
{
	CellState* startCell	= &m_boardState[GetCellItr(a_move.startCell)];
	CellState* endCell		= &m_boardState[GetCellItr(a_move.endCell)];

	// Check if this move is a promotion move
	CellState newCellState = *startCell;
	if (a_move.endCell.y == 0 && newCellState == CellState::DARK_MEN)
	{
		newCellState = CellState::DARK_KING;
	}
	else if (a_move.endCell.y == 7 && newCellState == CellState::LIGHT_MEN)
	{
		newCellState = CellState::LIGHT_KING;
	}

	(*endCell)		= newCellState;
	(*startCell)	= CellState::UNOCCUPIED;

	// Handle jump moves
	if (a_move.jumpCell != sf::Vector2i(-1, -1))
	{
		CellState* jumpCell = &m_boardState[GetCellItr(a_move.jumpCell)];
		(*jumpCell) = CellState::UNOCCUPIED;

		m_lastJumpPosition = a_move.endCell;

		return;
	}

	// Switch turns
	m_currentTurn = (m_currentTurn == State::PLAYER_ONE) ?
		State::PLAYER_TWO : State::PLAYER_ONE;
}

CheckersBoard* CheckersBoard::Clone() const
{
	CheckersBoard* board = new CheckersBoard();

	for (unsigned int i = 0; i < m_boardState.size(); ++i)
	{
		board->m_boardState[i] = m_boardState[i];
	}

	board->m_currentTurn		= m_currentTurn;
	board->m_lastJumpPosition	= m_lastJumpPosition;

	return board;
}
