#pragma once
#include <string>
#include <RakNet\RakPeerInterface.h>
#include <RakNet\BitStream.h>

class NetworkInterface
{
private:
	enum NetworkMessages
	{

	};

	std::string m_ip;
	short m_port;

	RakNet::RakPeerInterface* m_pPeerInterface;
	RakNet::SocketDescriptor m_sd;

	void HandleMessages();

public:
	NetworkInterface(std::string a_ip, short a_port);
	~NetworkInterface();

	inline short GetPort() const { return m_port; }
	inline std::string GetIP() const { return m_ip; }
};
