#include "NetworkInterface.h"
#include <RakNet\MessageIdentifiers.h>

#if DEBUG
#include <iostream>
#endif


NetworkInterface::NetworkInterface(std::string a_ip, short a_port)
	: m_pPeerInterface(nullptr),
	  m_port(a_port),
	  m_sd(m_port, a_ip.c_str())
{
	m_pPeerInterface = RakNet::RakPeerInterface::GetInstance();

	m_pPeerInterface->Startup(1, &m_sd, 1);
	m_pPeerInterface->SetMaximumIncomingConnections(1);


}

NetworkInterface::~NetworkInterface()
{
	delete m_pPeerInterface;
}

void NetworkInterface::HandleMessages()
{
	RakNet::Packet* packet = nullptr;

	while (true)
	{
		for (packet = m_pPeerInterface->Receive();
			m_pPeerInterface->DeallocatePacket(packet),
			packet = m_pPeerInterface->Receive(); )
		{
			switch (packet->data[0])
			{
			case ID_NEW_INCOMING_CONNECTION:
				// Handle new connection
				break;

			case ID_DISCONNECTION_NOTIFICATION:
				// Handle disconnection
				break;

			case ID_CONNECTION_LOST:
				// Handle client losing connection
				break;

#if DEBUG
			default:
				std::cout << "Recieved a network message with an unknown id: " << std::to_string(packet->data[0]) << std::endl;
				break;
#endif
			}
		}
	}
}
