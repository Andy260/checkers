#pragma once

enum class State;
struct BoardMove;
class CheckersBoard;

class AI
{
protected:
	State m_player;				// Defines which player this AI is

	AI(const State& a_player);

	virtual ~AI();

	// Makes a decision for a move within the given checkers game
	virtual BoardMove MakeDecision(CheckersBoard& a_board) = 0;

public:
	// Updates the AI, and makes it's the AI's turn on the given board
	void Update(CheckersBoard& a_board);

	// Returns which player this AI is
	inline State GetPlayer() const { return m_player; }
};
