#include "MCTS.h"
#include <thread>
#include <iostream>
#include <Board\CheckersBoard.h>
#include <Utilities\Timer.h>

typedef std::chrono::duration<double> seconds;


MCTS::MCTS(unsigned int a_playouts, const State& a_player)
	: AI(a_player), m_playouts(a_playouts), 
	  m_decisionThreads(m_playouts)
{
	// Seed random with current time
	srand((unsigned int)std::chrono::seconds().count());

	m_weighting.m_win				= 10;
	m_weighting.m_loss				= -10;
	m_weighting.m_draw				= 5;
	m_weighting.m_promotion			= 6;
	m_weighting.m_enemyPromotion	= -m_weighting.m_enemyPromotion;
	m_weighting.m_enemyJump			= -20;
}

BoardMove MCTS::MakeDecision(CheckersBoard& a_board)
{
	// Start timer
	Timer timer(true);

	std::vector<BoardMove> possibleMoves; 
	a_board.GetValidActions(possibleMoves);

	// Only one possible move can be made
	if (possibleMoves.size() == 1)
	{
		return possibleMoves[0];
	}

	// Construct the decision list
	std::vector<Decision> decisionList;
	CreateDecisionList(possibleMoves, decisionList);

	// Calculate decision scores
	m_decisionThreads.clear();

	Timer threadTime(true);

	// Create threads for each avaliable decision
	for (auto &decision : decisionList)
	{
		m_decisionThreads.push_back(
			std::thread(&MCTS::CalculateDecisionScore,
						this, &decision, &a_board, timer));
	}

	std::cout << "Thread creation time: " << threadTime.Elapsed().count() << " seconds" << std::endl;

	std::cout << "Total Number of threads: " << m_decisionThreads.size() << std::endl;

	// Wait for threads to complete execution
	for (auto &thread : m_decisionThreads)
	{
		thread.join();
	}

	std::cout << "Total Thread Time: " << threadTime.Elapsed().count() << " seconds" << std::endl;

	// Choose best decision based upon highest score
	std::sort(decisionList.begin(), decisionList.end(), 
		[](Decision a_left, Decision a_right)
	{
		return a_left.score > a_right.score;
	});

	std::cout << "MCTS Time: " << timer.Elapsed().count() << " seconds" << std::endl;

	return decisionList[0].move;
}

void MCTS::CreateDecisionList(std::vector<BoardMove>& a_possibleMoves,
							  std::vector<Decision>& a_decisionList)
{
	for (unsigned int i = 0; i < a_possibleMoves.size(); ++i)
	{
		a_decisionList.push_back(Decision(a_possibleMoves[i], 0));
	}
}

void MCTS::CalculateDecisionScore(Decision* a_pDecision, CheckersBoard* a_pBoard,
								  const Timer& a_timer)
{
	for (unsigned int i = 0; i < m_playouts; ++i)
	{
		// Ensure AI decision time is not exceeded
		if (a_timer.Elapsed() > seconds(1.0))
		{
			break;
		}

		// Clone current board state
		CheckersBoard* boardClone = a_pBoard->Clone();

		// Peform given action
		boardClone->PerformAction(a_pDecision->move);

		// Precalculate decision score
		a_pDecision->score += GetInitDecisionScore(boardClone);

		// Simulate game for this initial decision until a winner is found
		// (chooses random actions for both players)
		std::vector<BoardMove> possibleMoves;
		while (boardClone->GetState() == State::UNKNOWN)
		{
			boardClone->GetValidActions(possibleMoves);

			int moveItr = rand() % possibleMoves.size();
			boardClone->PerformAction(possibleMoves[moveItr]);
		}

		// Give a score to this action
		a_pDecision->score += GetDecisionScore(boardClone);

		// Clean up
		delete boardClone;
	}
}

int MCTS::GetInitDecisionScore(CheckersBoard* a_pBoard)
{
	CheckersBoard* boardClone = a_pBoard->Clone();

	std::vector<BoardMove> possibleMoves;
	boardClone->GetValidActions(possibleMoves);

	if (possibleMoves.size() >= 1 &&
		possibleMoves[0].jumpCell != sf::Vector2i(-1, -1))
	{
		int finalScore = 0;

		while (boardClone->GetCurrentTurn() != m_player)
		{
			// Get valid moves
			possibleMoves.clear();
			boardClone->GetValidActions(possibleMoves);

			// Sanity check for devision by zero
			if (possibleMoves.size() == 0)
			{
				break;
			}
			
			int moveItr = rand() % possibleMoves.size();
			boardClone->PerformAction(possibleMoves[moveItr]);

			finalScore += m_weighting.m_enemyJump;
		}

		return finalScore;
	}

	return 0;
}

int MCTS::GetDecisionScore(CheckersBoard* a_pBoard)
{
	State boardState = a_pBoard->GetState();

	int score = 0;

	if (boardState == m_player)
	{
		score += m_weighting.m_win;
	}
	else if (boardState == State::DRAW)
	{
		score += m_weighting.m_draw;
	}
	else
	{
		score -= m_weighting.m_loss;
	}

	return score;
}
