#pragma once
#include "AI.h"
#include <vector>
#include <thread>
#include <Board\CheckersBoard.h>

class Timer;

class MCTS : public AI
{
private:
	// Defines a potential deicsion and it's associated move
	struct Decision
	{
		BoardMove move;
		float score;

		Decision(const BoardMove& a_move, float a_score)
			: move(a_move), score(a_score) {}
	};

	// Defines decision weighting, 
	// dependant on the last action taken on the board
	struct DecisionWeighting
	{
		int m_win;
		int m_loss;
		int m_draw;
		int m_promotion;
		int m_enemyPromotion;
		int m_enemyJump;
	};

	DecisionWeighting m_weighting;				// Weighting for all decisions

	unsigned int m_playouts;					// The amount of games this AI will simulate before making a move

	std::vector<std::thread> m_decisionThreads; // Contains all threads used to calculate a decision

	// Recursive function which calculates the potential score for a decision
	// (could take a long time to complete, without a depth limit)
	void CalculateDecisionScore(Decision* a_pDecision, CheckersBoard* a_pBoard,
								const Timer& a_timer);

	// Creates a decision list for calculating the final scores for 
	// each decision
	void CreateDecisionList(std::vector<BoardMove>& a_possibleMoves,
							std::vector<Decision>& a_decisionList);

	int GetInitDecisionScore(CheckersBoard* a_pBoard);

	// Returns a score for the current state of the board and its 
	// associated decision
	int GetDecisionScore(CheckersBoard* a_pBoard);

public:
	MCTS(unsigned int a_playouts, const State& a_player);
	~MCTS() override {}

	// Returns the best decision that this AI is able to calculate
	BoardMove MakeDecision(CheckersBoard& a_board) override;
};
