#include <AI\AI.h>
#include <Board\CheckersBoard.h>


AI::AI(const State& a_player)
	: m_player(a_player)
{

}

AI::~AI()
{

}

void AI::Update(CheckersBoard& a_board)
{
	if (a_board.GetCurrentTurn() != m_player)
	{
		// Not AI's turn
		return;
	}

	std::vector<BoardMove> possibleMoves;
	a_board.GetValidActions(possibleMoves);

	if (a_board.GetCurrentTurn() != m_player)
	{
		return;
	}

	a_board.PerformAction(MakeDecision(a_board));
}